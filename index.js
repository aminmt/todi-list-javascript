//selectors
const todoInput = document.querySelector('.todo-input');
const todoButton = document.querySelector('.todo-button');
const todoLit = document.querySelector('.todoList');
const filterOption = document.querySelector('.filter-todos');
//event listeners
todoButton.addEventListener('click', addTodo);
todoLit.addEventListener('click', checkRemove);
filterOption.addEventListener('click', filterTodos);
// functions
function addTodo(e) {
    e.preventDefault();
    const todoDiv = document.createElement('div');

    todoDiv.classList.add('todo');
    const newTodo = `
    <li>${todoInput.value}</li>
    <span>
        <i class="far fa-check-square"></i>
    </span>
    <span>
        <i class="far fa-trash-alt"></i>
    </span>`;
    todoDiv.innerHTML = newTodo;
    todoLit.appendChild(todoDiv);
    todoInput.value = ''

}

function checkRemove(e) {
    const classList = [...e.target.classList];
    const item = e.target;
    if (classList[1] === 'fa-check-square') {
        const todo = item.parentElement.parentElement;
        todo.classList.toggle('completed');
    } else if (classList[1] === 'fa-trash-alt') {
        const todo = item.parentElement.parentElement;
        todo.remove();
    }
}

function filterTodos(e) {
    const todos = [...todoLit.childNodes];
    todos.forEach((todo) => {
        console.log(todo)
        switch (e.target.value) {
            case 'all':
                todo.style.display = 'flex';
                break;
            case 'completed':
                if (todo.classList.contains('completed')) {
                    todo.style.display = 'flex';
                } else {
                    todo.style.display = 'none';
                }
                break;
            case 'unComplted':
                if (!todo.classList.contains('completed')) {
                    todo.style.display = 'flex';
                } else {
                    todo.style.display = 'none';
                }
                break;
        }
    });

}



